#!/usr/bin/env bash

ITOP_URL='https://sourceforge.net/projects/itop/files/latest/download'
ITOP_TOOLKIT_URL='https://www.combodo.com/documentation/iTopDataModelToolkit-2.7.zip'

apt-get install -y php-xml php-soap php-ldap php-zip php-gd php-mbstring php-curl unzip graphviz

# Dev Tools
apt-get install -y git augeas-tools vim-nox

# Change php.ini
#set post_max_size 32M
augtool set /files/etc/php.ini/PHP/post_max_size 32M

cd /var/www/html

echo -ne "Adding redirect page..."
cat > index.html << EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="refresh" content="2; url=http://192.168.100.100/itop/" />
    <title>Redirecting to iTop...</title>
  </head>
  <body>
    <h3>Redirecting to iTop...</h3>
  </body>
</html>
EOF
echo -e "OK"

if [ -d "/var/www/html/itop" ]; then
	echo -ne "Cleaning previous install..."
	rm -rf /var/www/html/itop
	echo -e "OK"
fi

echo -ne "Downloading iTop..."
wget -q ${ITOP_URL} -O /tmp/itop.zip
echo -e "OK"

echo -ne "Unzipping & moving..."
unzip -qq /tmp/itop.zip
rm /tmp/itop.zip
rm INSTALL README LICENSE
mv -f web itop
echo -e "OK"

echo -ne "Creating database..."
mariadb -u root -e "DROP DATABASE IF EXISTS itop";
mariadb -u root -e "CREATE DATABASE IF NOT EXISTS itop";
mariadb -u root -e "CREATE USER IF NOT EXISTS 'itop'@'%' IDENTIFIED BY 'itop';"
mariadb -u root -e "GRANT ALL PRIVILEGES ON itop.* TO 'itop'@'%';"
mariadb -u root -e "FLUSH PRIVILEGES;"
echo -e "OK"

echo -ne "Downloading iTop toolkit..."
cd /var/www/html/itop
wget -q ${ITOP_TOOLKIT_URL} -O toolkit.zip
unzip -qq toolkit.zip
rm toolkit.zip
echo -e "OK"

chown www-data:www-data -R /var/www/html/itop
chmod -R ug+rw /var/www/html/itop

# restart apache
sudo service apache2 restart

echo ""
echo "#---------------------------------------------------------------------"
echo -e "Install complete. Open http://192.168.100.100/itop in your web browser"
echo "#---------------------------------------------------------------------"
echo ""
echo ""
