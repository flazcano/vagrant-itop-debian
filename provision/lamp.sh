#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

# update / upgrade
env

echo "set grub-pc/install_devices /dev/sda" | debconf-communicate
apt-get -y -qq update
apt-get -y -qq upgrade

# install apache, php and other tools
apt-get -y install apache2 php htop iotop

# enable mod_rewrite
a2enmod rewrite

# install mariadb and give password to installer
apt-get -y install mariadb-server
apt-get -y install php-mysql

# Add user vagrant to www-data group
adduser vagrant www-data

# restart apache
service apache2 restart
